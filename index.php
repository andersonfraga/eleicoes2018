<?php

//error_reporting(0);

chdir(getcwd());

require __DIR__ . '/src/helper.php';
require __DIR__ . '/src/App.php';

$app = new App;

$_ENV['disable_zero']  = false;
$_ENV['disable_cache'] = true;
$_ENV['cache_seconds'] = 60;

echo $app->run(
    $_GET['tipo']  ?? 'expandida',
    $_GET['cargo'] ?? '3',
    $_GET['local'] ?? 'rs'
);

//
// http://divulga.tse.jus.br/2018/divulgacao/oficial/295/distribuicao/br/br-c0001-e000295-041-f.zip
// http://divulga.tse.jus.br/2018/divulgacao/oficial/297/distribuicao/rs/rs-c0003-e000297-002-f.zip
// http://divulga.tse.jus.br/2018/divulgacao/oficial/297/distribuicao/rs/rs-c0005-e000297-003-f.zip
// http://divulga.tse.jus.br/2018/divulgacao/oficial/297/distribuicao/rs/rs-c0006-e000297-004-f.zip
// http://divulga.tse.jus.br/2018/divulgacao/oficial/297/distribuicao/rs/rs-c0007-e000297-004-f.zip
//
//////
// http://divulga.tse.jus.br/2018/divulgacao/oficial/295/fotos/br/280000605589.jpeg