<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
    <!-- <meta http-equiv="refresh" content="600" /> -->
    <meta http-equiv="pragma" content="no-cache" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="resource-type" content="document" />
    <meta name="classification" content="Internet" />
    <meta name="description" content="Portal de notícias do Grupo de Gazeta de Comunicações - Santa Cruz do Sul - RS - Notícias, esportes, blogs, entretenimento e os sites dos veículos do Grupo Gazeta." />
    <meta name="keywords" content="santa cruz do sul,notícias,esportes,vale do rio pardo,entretenimento,blogs,rádios,jornal,tempo,vídeos,gazeta do sul,gazeta da serra,radio gazeta,gazeta fm, gazeta am,radio rio pardo,viavale,fundação gazeta,agrobrasil,rio grande do sul,gaucho,futebol,economia,política,portal,jornal online,santa cruz,autódromo" />
    <meta name="robots" content="all" />
    <meta name="googlebot" content="all" />
    <meta name="distribution" content="Global" />
    <meta name="rating" content="General" />
    <meta name="author" content="Gazeta Grupo de Comunicações" />
    <meta name="copyright" content="Gazeta Grupo de Comunicações" />
    <meta name="language" content="pt-br" />
    <meta name="doc-class" content="Completed" />
    <meta name="doc-rights" content="Public" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta property="fb:app_id" content="1187767367906811" />
    <meta property="og:url" content="http://www.gaz.com.br/conteudos/resultados/index.php"/>

    <title>GAZ - Notícias da região para o mundo</title>
    <base href="http://www.gaz.com.br/"/>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,600' rel='stylesheet' type='text/css'>
    <link href="http://www.gaz.com.br/static/css/gaz-pw/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/font-awesome.min.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://www.gaz.com.br/static/css/gaz-pw/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="http://www.gaz.com.br/static/css/gaz-pw/gaz-pw-theme.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/style_paywall.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.5/css/weather-icons.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://www.gaz.com.br/static/js/gaz-pw/ie-emulation-modes-warning.js"></script>

    <link rel="shortcut icon" href="http://www.gaz.com.br/static/img/favicon.png" />
    <link rel="ogurl" href="http://www.gaz.com.br/conteudos/resultados/index.php">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <script type='text/javascript'>
      googletag.cmd.push(function() {
        googletag.defineSlot('/19167842/GAZ_160x600_nivel1', [160, 600], 'div-gpt-ad-1442905933153-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_300x250_nivel1', [300, 250], 'div-gpt-ad-1442905933153-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_300x250_nivel2', [300, 250], 'div-gpt-ad-1442905933153-2').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_728x90_nivel1', [728, 90], 'div-gpt-ad-1442905933153-3').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_728x90_nivel2', [728, 90], 'div-gpt-ad-1442905933153-4').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/Pop-up', [[400, 450], [450, 400]], 'div-gpt-ad-1442905933153-5').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        googletag.defineSlot('/19167842/GAZ_320x50_nivel1', [320, 50], 'div-gpt-ad-1443719839443-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_320x50_nivel2', [320, 50], 'div-gpt-ad-1443719839443-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        googletag.defineSlot('/19167842/Banner_120x60', [120, 60], 'div-gpt-ad-1463771394450-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        //googletag.defineSlot('/19167842/GAZ_1170x164_top_premium', [1170, 164], 'div-gpt-ad-1445259304236-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        //googletag.defineSlot('/19167842/GAZ_320x45_top_premium', [320, 45], 'div-gpt-ad-1445259304236-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());


         var mapping = googletag.sizeMapping().
            addSize([320, 200], [320, 45]).
            addSize([360, 200], [320, 45]).
            addSize([480, 200], [320, 45]).
            addSize([768, 200], [728, 90]).
            addSize([800, 200], [728, 90]).
            addSize([1024, 200], [728, 90]).
            addSize([1140, 200], [1170, 164]).
            build();

        window.slot1= googletag.defineSlot('/19167842/GAZ_1170x164_top_premium', [1170, 164], 'div-gpt-ad-1445259304236-0').
              setTargeting('PAGINA', ['']).
              defineSizeMapping(mapping).
              addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.pubads().setTargeting('SITE', ['GAZ']);
        googletag.enableServices();
      });

      // <![CDATA[
      var resizeTimer; function resizer() { googletag.pubads().refresh([window.slot1]); }
      window.addEventListener("resize", function(){
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizer, 250);
      });
      // ]]&gt;

    </script>

  </head>


  <body role="document">

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-8843239-2', 'auto');
      ga('send', 'pageview');
    </script>

    <div id="fb-root"></div>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=1187767367906811";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>