<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="imagetoolbar" content="no" />
    <meta http-equiv="Content-Type" content="text/html" charset="UTF-8" />
    <!-- <meta http-equiv="refresh" content="600" /> -->
    <meta http-equiv="pragma" content="no-cache" />

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="resource-type" content="document" />
    <meta name="classification" content="Internet" />
    <meta name="description" content="Portal de notícias do Grupo de Gazeta de Comunicações - Santa Cruz do Sul - RS - Notícias, esportes, blogs, entretenimento e os sites dos veículos do Grupo Gazeta." />
    <meta name="keywords" content="santa cruz do sul,notícias,esportes,vale do rio pardo,entretenimento,blogs,rádios,jornal,tempo,vídeos,gazeta do sul,gazeta da serra,radio gazeta,gazeta fm, gazeta am,radio rio pardo,viavale,fundação gazeta,agrobrasil,rio grande do sul,gaucho,futebol,economia,política,portal,jornal online,santa cruz,autódromo" />
    <meta name="robots" content="all" />
    <meta name="googlebot" content="all" />
    <meta name="distribution" content="Global" />
    <meta name="rating" content="General" />
    <meta name="author" content="Gazeta Grupo de Comunicações" />
    <meta name="copyright" content="Gazeta Grupo de Comunicações" />
    <meta name="language" content="pt-br" />
    <meta name="doc-class" content="Completed" />
    <meta name="doc-rights" content="Public" />
    <meta name="MSSmartTagsPreventParsing" content="true" />

    <meta property="fb:app_id" content="1187767367906811" />
    <meta property="og:url" content="http://www.gaz.com.br/conteudos/resultados/index.php"/>

    <title>GAZ - Notícias da região para o mundo</title>

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,600' rel='stylesheet' type='text/css'>
    <link href="http://www.gaz.com.br/static/css/gaz-pw/bootstrap.min.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/font-awesome.min.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/bootstrap-theme.min.css" rel="stylesheet">
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="http://www.gaz.com.br/static/css/gaz-pw/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="http://www.gaz.com.br/static/css/gaz-pw/gaz-pw-theme.css" rel="stylesheet">
    <link href="http://www.gaz.com.br/static/css/gaz-pw/style_paywall.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/weather-icons/2.0.5/css/weather-icons.min.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="http://www.gaz.com.br/static/js/gaz-pw/ie-emulation-modes-warning.js"></script>

    <link rel="shortcut icon" href="http://www.gaz.com.br/static/img/favicon.png" />
    <link rel="ogurl" href="http://www.gaz.com.br/conteudos/resultados/index.php">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        /* wait for jquery to load */
        window.wait4j=function(a){if(window.jQuery){a.call(this);}else{setTimeout(function(){window.wait4j(a);},1);}};
    </script>

    <script type='text/javascript'>
      var googletag = googletag || {};
      googletag.cmd = googletag.cmd || [];
      (function() {
        var gads = document.createElement('script');
        gads.async = true;
        gads.type = 'text/javascript';
        var useSSL = 'https:' == document.location.protocol;
        gads.src = (useSSL ? 'https:' : 'http:') +
          '//www.googletagservices.com/tag/js/gpt.js';
        var node = document.getElementsByTagName('script')[0];
        node.parentNode.insertBefore(gads, node);
      })();
    </script>

    <script type='text/javascript'>
      googletag.cmd.push(function() {
        googletag.defineSlot('/19167842/GAZ_160x600_nivel1', [160, 600], 'div-gpt-ad-1442905933153-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_300x250_nivel1', [300, 250], 'div-gpt-ad-1442905933153-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_300x250_nivel2', [300, 250], 'div-gpt-ad-1442905933153-2').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_728x90_nivel1', [728, 90], 'div-gpt-ad-1442905933153-3').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_728x90_nivel2', [728, 90], 'div-gpt-ad-1442905933153-4').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/Pop-up', [[400, 450], [450, 400]], 'div-gpt-ad-1442905933153-5').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        googletag.defineSlot('/19167842/GAZ_320x50_nivel1', [320, 50], 'div-gpt-ad-1443719839443-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        googletag.defineSlot('/19167842/GAZ_320x50_nivel2', [320, 50], 'div-gpt-ad-1443719839443-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        googletag.defineSlot('/19167842/Banner_120x60', [120, 60], 'div-gpt-ad-1463771394450-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());

        //googletag.defineSlot('/19167842/GAZ_1170x164_top_premium', [1170, 164], 'div-gpt-ad-1445259304236-0').setTargeting('PAGINA', ['']).addService(googletag.pubads());
        //googletag.defineSlot('/19167842/GAZ_320x45_top_premium', [320, 45], 'div-gpt-ad-1445259304236-1').setTargeting('PAGINA', ['']).addService(googletag.pubads());


         var mapping = googletag.sizeMapping().
            addSize([320, 200], [320, 45]).
            addSize([360, 200], [320, 45]).
            addSize([480, 200], [320, 45]).
            addSize([768, 200], [728, 90]).
            addSize([800, 200], [728, 90]).
            addSize([1024, 200], [728, 90]).
            addSize([1140, 200], [1170, 164]).
            build();

        window.slot1= googletag.defineSlot('/19167842/GAZ_1170x164_top_premium', [1170, 164], 'div-gpt-ad-1445259304236-0').
              setTargeting('PAGINA', ['']).
              defineSizeMapping(mapping).
              addService(googletag.pubads());

        googletag.pubads().enableSingleRequest();
        googletag.pubads().setTargeting('SITE', ['GAZ']);
        googletag.enableServices();
      });

      // <![CDATA[
      var resizeTimer; function resizer() { googletag.pubads().refresh([window.slot1]); }
      window.addEventListener("resize", function(){
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizer, 250);
      });
      // ]]&gt;

    </script>

  <script type="text/javascript" charset="utf-8">

    function showListCidades() {
        $("#eleicoesDialog").modal("show");

        if ($("#eleicoesDialogContent").attr("data-tinx-loaded") !== "1") {
            $.ajax("/cidades.php", {
                headers: {
                    'Content-Type': 'application/json'
                },
                beforeSend: function () {
                    $('#divLoadingData').show();
                },
                success: function (data) {
                    $("#eleicoesDialogContent").attr("data-tinx-loaded", "1");
                    $('#eleicoesDialogContent').html(data);
                },
                error: function () {
                    // $('#notification-bar').text('An error occurred');
                }
            });
        }
    }

  </script>
  </head>


  <body role="document">

<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-8843239-2', 'auto');
      ga('send', 'pageview');
    </script>

    <div id="fb-root"></div>
    <script>
      (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.4&appId=1187767367906811";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    </script>


<?php
//USADO NO CSE DO GOOGLE PARA A BUSCA INFORMANDO O CODIGO CSE DA PAGINA
$cod_cse ='011746485525548084497:ya-_o1web6k';
?>
<?php
// verificando se esta na página conteúdo de marca
// se estiver nào exibe banner do dfp
$url_atual_pag = $_SERVER["REQUEST_URI"];
$str_procurada = 'conteudo_de_marca';

$pos = strpos($url_atual_pag, $str_procurada);
?>

<!-- Material Design Bootstrap -->
<link href="http://www.gaz.com.br/static/css/style_sidebar.css?v7" rel="stylesheet">

<div id="menu-slider" class="hidden-sn mdb-skin">
    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 mdb-sidenav">
            <ul class="custom-scrollbar list-unstyled" style="max-height:100vh;">
                <li>
                    <!-- <form class="search-form" role="search"> -->
                        <div class="search-form form-group md-form mt-0 pt-1 waves-light">
                            <input id="busca-slide" type="text" class="form-control busca_desk" placeholder="Buscar no Gaz">
                            <button class="icon_search"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                    <!-- </form> -->
                </li>
                <!--/.Search Form-->
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-volume-up" aria-hidden="true"></i><strong>Rádios ao vivo</strong><i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a class="waves-effect" title="Gazeta AM" href="javascript:void(centerPopupRadios('http://www.grupogaz.com.br/popup_radio.php?id=5','POPUP',370,750));" ><i class="fa fa-volume-up" aria-hidden="true"></i>Gazeta AM</a></li>
                                    <li><a class="waves-effect" title="Gazeta FM" href="javascript:void(centerPopupRadios('http://www.grupogaz.com.br/popup_radio.php?id=4','POPUP',370,750));" ><i class="fa fa-volume-up" aria-hidden="true"></i>Gazeta FM</a></li>
                                    <li><a class="waves-effect" title="99.7 FM" href="javascript:void(centerPopupRadios('http://www.grupogaz.com.br/popup_radio.php?id=9','POPUP',370,750));" ><i class="fa fa-volume-up" aria-hidden="true"></i>99.7 FM</a></li>
                                    <li><a class="waves-effect" title="Rádio Rio Pardo" href="javascript:void(centerPopupRadios('http://www.grupogaz.com.br/popup_radio.php?id=7','POPUP',370,750));" ><i class="fa fa-volume-up" aria-hidden="true"></i>Rádio Rio Pardo</a></li>
                                    <li><a class="waves-effect" title="FM Sobradinho" href="javascript:void(centerPopupRadios('http://www.grupogaz.com.br/popup_radio.php?id=6','POPUP',370,750));" ><i class="fa fa-volume-up" aria-hidden="true"></i>FM Sobradinho</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r">Notícias<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a class="waves-effect" title="Ver todas de notícias" href="conteudos/noticias">Ver todas</a></li>
                                    <li><a class="waves-effect" title="Gaz Centro-Serra" href="conteudos/centro_serra">Centro-Serra</a></li>
                                    <li><a class="waves-effect" title="Concursos" href="conteudos/concursos">Concursos</a></li>
                                    <li><a class="waves-effect" title="Educação" href="conteudos/educacao">Educação</a></li>
                                    <li><a class="waves-effect" title="Eleições" href="conteudos/eleicoes/">Eleições</a></li>
                                    <li><a class="waves-effect" title="Gazeta Explica" href="conteudos/gazeta_explica">Gazeta Explica</a></li>
                                    <li><a class="waves-effect" title="Geral" href="conteudos/geral">Geral</a></li>
                                    <li><a class="waves-effect" title="Mundo" href="conteudos/mundo">Mundo</a></li>
                                    <li><a title="Oktoberfest" href="conteudos/oktoberfest">Oktoberfest</a></li>
                                    <li><a class="waves-effect" title="Polícia" href="conteudos/policia">Polícia</a></li>
                                    <li><a class="waves-effect" title="Política" href="conteudos/politica">Política</a></li>
                                    <li><a class="waves-effect" title="Regional" href="conteudos/regional">Regional</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r">Esportes<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a class="waves-effect" title="Ver todas de esporte" href="conteudos/esportes">Ver todas</a></li>
                                    <li><a class="waves-effect" title="Copa 2018" href="conteudos/copa_2018">Copa 2018</a></li>
                                    <li><a class="waves-effect" title="Esporte local" href="conteudos/esporte_local">Esporte local</a></li>
                                    <li><a class="waves-effect" title="Campeonato Brasileiro" href="conteudos/futebol">Futebol</a></li>
                                    <li><a class="waves-effect" title="Grêmio" href="conteudos/gremio">Grêmio</a></li>
                                    <li><a class="waves-effect" title="Internacional" href="conteudos/internacional">Internacional</a></li>
                                    <li><a class="waves-effect" title="Outros esportes" href="conteudos/outros_esportes">Outros esportes</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r">Variedades<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a class="waves-effect" title="Ver todas de variedades" href="conteudos/variedades">Ver todas</a></li>
                                    <li><a class="waves-effect" title="Gaz Centro-Serra" href="conteudos/blogs">Blogs</a></li>
                                    <li><a class="waves-effect" title="Colunistas" href="conteudos/colunistas">Colunistas</a></li>
                                    <li><a class="waves-effect" title="Cinemas e séries" href="conteudos/cinema">Cinema e séries</a></li>
                                    <li><a class="waves-effect" title="Classificados" href="https://assinaturas.gaz.com.br/classificados" target="_blank">Classificados</a></li>
                                    <li><a class="waves-effect" title="Em cartaz" href="conteudos/em_cartaz">Em cartaz</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r">Elas por Elas<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a class="waves-effect" title="Moda" href="http://elas.gaz.com.br/conteudos/moda">Moda</a></li>
                                    <li><a class="waves-effect" title="Beleza" href="http://elas.gaz.com.br/conteudos/beleza">Beleza</a></li>
                                    <li><a class="waves-effect" title="Casa e Decoração" href="http://elas.gaz.com.br/conteudos/casa_e_decoracao">Casa e Decoração</a></li>
                                    <li><a class="waves-effect" title="Comportamento" href="http://elas.gaz.com.br/conteudos/comportamento" target="_blank">Comportamento</a></li>
                                    <li><a class="waves-effect" title="Saúde" href="http://elas.gaz.com.br/conteudos/saude" target="_blank">Saúde</a></li>
                                    <li><a class="waves-effect" title="Noivas" href="http://elas.gaz.com.br/conteudos/noivas">Noivas</a></li>
                                    <li><a class="waves-effect" title="Sexo" href="http://elas.gaz.com.br/conteudos/sexo">Sexo</a></li>
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a class="waves-effect" title="Últimas notícias" href="http://www.gaz.com.br/conteudos/ultimas">Últimas<i class="fa fa-angle-down rotate-icon"></i></a>
                        </li>
                    </ul>

                    <ul class="collapsible collapsible-accordion">
                        <li style="border: 1px solid #fff;border-right: 0;border-left: 0;padding: 5px 0;">
                            <div class="collapsible-body" style="display:block">
                                <div class="row no-espacamento">
                                  <div class="col-md-12">
                                    <iframe width="100%" id="iframe-previsao"  class="embed-responsive-item" src="http://www.gaz.com.br/previsao-do-tempo/index-cabecalho-gaz.php" frameborder="0" height="60"></iframe>
                                  </div>
                                </div>
                            </div>
                        </li>
                        <li style="margin: 5px 0;">
                            <div class="collapsible-body" style="display:block">
                                <div class="row no-espacamento">
                                    <div class="col-md-12">
                                        <iframe class="iframe-edicao-jornal" frameborder="0" src="https://assinaturas.gaz.com.br/arquivos_edicoes/img_ultima_edicao_gzt_sul.html.php" scrolling="no" style="width: 100%; height: 220px; padding: 0; margin: 0;"></iframe></div>
                                        <h5 style="text-align: center; margin: 0px auto;">EDIÇÃO IMPRESSA</h5>
                                </div>
                            </div>
                        </li>

                    </ul>
                </li>
                <!--/. Side navigation links -->

                <li>
                    <div class="row no-espacamento"><div class="col-md-12">&nbsp;</div></div>
                    <div class="sidenav-bg mask-strong"></div>
                </li>
              </ul>
          </div>


    </header>
    <!--/.Double navigation-->
</div>

<section class="gaz-section-cabecalho">
    <div class="container">

    <div class="row gaz-header">

        <div class="col-xs-1 col-sm-4 col-md-4 menu-bar color-white">
         <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
        </div>

        <div class="col-xs-3 col-sm-3 col-md-3 col-md-offset-1">
            <div class="gaz-logo hidden-sm hidden-md hidden-lg">
                <a href="index.php" title="Gaz - Sua Gazeta Online">
                    <img class="" src="http://www.gaz.com.br/static/img/gaz-pw/gaz_logo_mobile.png" alt="Portal Gaz">
                </a>
            </div>
            <div class="gaz-logo gaz-logo-desktop visible-sm visible-md visible-lg">
                <a href="index.php" title="Gaz - Sua Gazeta Online">
                    <img class="" src="http://www.gaz.com.br/static/img/gaz-pw/gaz_logo_mobile.png" alt="Portal Gaz">
                </a>
            </div>
        </div>

    <!-- <div class="col-md-4 hidden-xs hidden-sm" style="text-align: center;">
        <iframe src="http://www.gaz.com.br/previsao-do-tempo/index-cabecalho-gaz.php" frameborder="0" height="60"></iframe>
    </div> -->

      <div class="col-xs-5 col-sm-4 col-md-3">
        <?php if(isset($_SESSION['NOME_USUARIO_PORTAL']) ) { ?>
          <div class="hidden-sm hidden-md hidden-lg bt-user-login color-white"><a href="#" data-toggle="modal" data-target=".login-modal"><i class="glyphicon glyphicon-user"></i> Bem-vindo(a)</a></div>
        <?php }else{ ?>
          <div class="hidden-sm hidden-md hidden-lg bt-user-login color-white"><a href="#" data-toggle="modal" data-target=".login-modal"><i class="glyphicon glyphicon-user"></i> Entrar</a></div>
        <?php } ?>

            <?php //mobile modal usuario login ?>
            <div class="modal fade login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header user-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="ModalLabel">Área de login</h4>
                  </div>
                  <div class="modal-body user-body">
                    <?php if(isset($_SESSION['ID_USUARIO_PORTAL']) ) { ?>
                        <p><?php echo $_SESSION['NOME_USUARIO_PORTAL']; ?></p>
                        <p><?php echo $_SESSION['EMAIL_USUARIO_PORTAL']; ?></p>
                    <?php }else{ ?>
                        <span class="texto_aviso_login_user_mobile hide"></span>
                        <form name="form_login_user" id="form_login_user_mobile" action="" method="POST" accept-charset="utf-8">
                                <input type="hidden" name="url_atual" id="url_atual" value="">
                                <input type="hidden" name="methodo" id="methodo" value="login">

                                <div class="row"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group input-group-md input_form">
                                            <!-- <span class="input-group-addon" id="sizing-addon1">E-mail</span> -->
                                            <input required="required" type="email" id="email_user_login_user_mobile" name="EMAIL_USUARIO_PORTAL" class="form-control required" placeholder="Digite seu e-mail" aria-describedby="sizing-addon1">
                                        </div>
                                        <br/>
                                        <div class="input-group input-group-md input_form">
                                            <input required="required" type="password" name="SENHA_USUARIO_PORTAL" class="form-control required" id="senha_user_login_user_mobile" placeholder="Digite sua senha">
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-7">
                                                <div class="text-center">
                                                    <a class="links_texto" title="Esqueceu sua senha? Clique aqui para criar uma nova!" href="https://assinaturas.gaz.com.br/usuario/lembrarsenha" target="_blank">Esqueci minha senha</a>
                                                </div>
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="input-group input-group-lg">
                                                    <a class="links_texto" title="Ainda não possui usuário? Clique aqui e crie o seu!" href="https://assinaturas.gaz.com.br/usuario/cadastro" target="_blank">Criar usuário</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                          <div class="pull-right">
                                            <input id="enviar_login_user_mobile" type="submit" name="btnSubmit" value="Entrar" class="btn btn-default btn-flat">
                                          </div>

                                          <div id="enviando_login_user_mobile" class="hide"></div>

                                        </div>
                                    </div>

                                </div>
                            </form>

                    <?php } ?>

                  </div>
                  <div class="modal-footer user-footer">

                    <!-- <div class="pull-left">
                      <a class="btn btn-default btn-flat" href="#">Alterar dados</a>
                    </div> -->
                    <div class="pull-right">
                        <?php if(isset($_SESSION['ID_USUARIO_PORTAL']) ) { ?>
                          <form id="logout_user_mobile" name="logout_user" action="" method="POST" accept-charset="utf-8">
                            <input type="hidden" name="methodo" id="methodo" value="logout">
                            <input id="sair_login_user_mobile" type="submit" name="btnSubmit" value="Sair" class="btn btn-default btn-flat">
                          </form>
                          <!-- <a id="logout_user" class="btn btn-default btn-flat" href="#">Sair</a> -->
                        <?php }  ?>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <?php //fim mobile modal usuario login ?>

            <?php //desktop modal usuario login ?>
            <ul class="hidden-xs nav navbar-nav navbar-right gaz-menu-user" id="">

              <li class="gaz-menu-user-item">

                <a data-toggle="dropdown" class="dropdown-toggle color-white" href="#">
                  <!-- <i class="glyphicon glyphicon-user"></i> -->
                  <?php if(isset($_SESSION['NOME_USUARIO_PORTAL']) ) {
                      $arr_nome_abrv = explode(' ', $_SESSION['NOME_USUARIO_PORTAL']);
                      if(isset($arr_nome_abrv[0])){

                        if(strlen($arr_nome_abrv[0] > 25) ) {
                            $nome_abrv = 'Bem-vindo(a)';
                        }else{
                            $nome_abrv = 'Olá '.ucwords( strtolower($arr_nome_abrv[0]) );
                        }
                      }else{
                        $nome_abrv = 'Bem-vindo(a)';
                      }?>

                    <span><?php echo $nome_abrv; ?><i class="caret"></i></span>

                  <?php }else{ ?>
                    <i class="glyphicon glyphicon-user"></i> Entrar
                  <?php } ?>

                </a>
                <ul class="dropdown-menu">

                    <li class="user-header">
                      Área de login
                    </li>

                    <li class="user-body">
                      <!-- <img alt="User Image" class="img-circle" src="http://lorempixel.com/65/65"> -->
                      <?php if(isset($_SESSION['ID_USUARIO_PORTAL']) ) { ?>
                        <p><?php echo $_SESSION['NOME_USUARIO_PORTAL']; ?></p>
                        <p><?php echo $_SESSION['EMAIL_USUARIO_PORTAL']; ?></p>

                      <?php }else{ ?>
                          <span class="texto_aviso_login_user hide"></span>
                          <form name="form_login_user" id="form_login_user" action="" method="POST" accept-charset="utf-8">
                                  <input type="hidden" name="url_atual" id="url_atual" value="">
                                  <input type="hidden" name="methodo" id="methodo" value="login">

                                  <div class="row"></div>
                                  <div class="row">
                                      <div class="col-md-12">
                                          <div class="input-group input-group-md input_form">
                                              <!-- <span class="input-group-addon" id="sizing-addon1">E-mail</span> -->
                                              <input required="required" type="email" id="email_user_login_user" name="EMAIL_USUARIO_PORTAL" class="form-control required" placeholder="Digite seu e-mail" aria-describedby="sizing-addon1">
                                          </div>
                                          <br/>
                                          <div class="input-group input-group-md input_form">
                                              <input required="required" type="password" name="SENHA_USUARIO_PORTAL" class="form-control required" id="senha_user_login_user" placeholder="Digite sua senha">
                                          </div>

                                          <div class="row">
                                              <div class="col-xs-7">
                                                  <div class="text-center">
                                                      <a class="links_texto" title="Esqueceu sua senha? Clique aqui para criar uma nova!" href="https://assinaturas.gaz.com.br/usuario/lembrarsenha" target="_blank">Esqueci minha senha</a>
                                                  </div>
                                              </div>
                                              <div class="col-xs-5">
                                                  <div class="input-group input-group-lg">
                                                      <a class="links_texto" title="Ainda não possui usuário? Clique aqui e crie o seu!" href="https://assinaturas.gaz.com.br/usuario/cadastro" target="_blank">Criar usuário</a>
                                                  </div>
                                              </div>
                                          </div>

                                          <div class="form-group">

                                            <div class="pull-right">
                                              <input id="enviar_login_user" type="submit" name="btnSubmit" value="Entrar" class="btn btn-default btn-flat">
                                            </div>

                                            <div id="enviando_login_user" class="hide"></div>

                                         </div>

                                      </div>

                                  </div>
                              </form>

                      <?php } ?>

                    </li>

                    <li class="user-footer">
                      <!-- <div class="pull-left">
                        <a class="btn btn-default btn-flat" href="#">Alterar dados</a>
                      </div> -->
                       <div class="pull-right">
                          <?php  if(isset($_SESSION['ID_USUARIO_PORTAL']) ) { ?>
                            <form id="logout_user" name="logout_user" action="" method="POST" accept-charset="utf-8">
                              <input type="hidden" name="methodo" id="methodo" value="logout">
                              <input id="sair_login_user" type="submit" name="btnSubmit" value="Sair" class="btn btn-default btn-flat">
                            </form>
                            <!-- <a id="logout_user" class="btn btn-default btn-flat" href="#">Sair</a> -->
                          <?php }  ?>
                      </div>
                    </li>
                </ul>

              </li>

            </ul>
            <?php //fim desktop modal usuario login ?>

        </div>

        <div class="sign col-xs-2 col-sm-1 col-md-1">
            <a href="https://assinaturas.gaz.com.br/assinatura/index" class="btn-sign" target="_blank">Assine</a>
        </div>

    </div> <!-- row navbar -->

  </div> <!-- container -->

</section> <!-- .gaz-section-cabecalho -->

<?php if ($pos === false){ ?>

<div class="container-fluid hidden-sm hidden-md hidden-lg">
     <div class="row">
          <div style="padding: 0;margin: 0" class="col-md-12 hidden-sm hidden-md hidden-lg">
              <div class="banner_top_premium_mobile" style="text-align:center;">
                  <div style="width:100%;margin:0 auto;">
                    <!-- /19167842/GAZ_320x100_mobile -->
                    <div id='div-gpt-ad-1524507517946-0' style='height:100px; width:320px;margin:0 auto;'>
                      <script>
                        googletag.cmd.push(function() { googletag.display('div-gpt-ad-1524507517946-0'); });
                      </script>
                    </div>
                  </div>
              </div>
          </div>
     </div>
</div>

<?php } ?>

<section class="gaz-section-intervalo"></section>


  <span id="banner_popup" class="hidden-xs" style="display:none;">
    <div style="z-index:999999; position:absolute; width:auto; height:auto; left:35%; top:45%; text-align: right;">
      <div style="width:100px; padding:0; margin:-32px 0 0 320px; background-color:#fff; border:0;" id="close_0">
        <a href="#" class="cl_popup_es_fechar">
          <span style="color: red; font-size: 40px;" class="glyphicon glyphicon-remove-circle"></span>
        </a>
      </div>
      <!-- /19167842/Pop-up -->
      <div id='div-gpt-ad-1442905933153-5'>
        <script type='text/javascript'>
          googletag.cmd.push(function() { googletag.display('div-gpt-ad-1442905933153-5'); });
        </script>
      </div>
    </div>
  </span>
  <script>
    (function() {
      var cx = '002541288498855708502:sb2ap3fa6sk';
      var gcse = document.createElement('script');
      gcse.type = 'text/javascript';
      gcse.async = true;
      gcse.src = 'https://cse.google.com/cse.js?cx=' + cx;
      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(gcse, s);
    })();
  </script>
  <gcse:search></gcse:search>
  <link href="http://www.gaz.com.br/static/css/styles_search_google.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script type="text/javascript">
    function valorInputBusca(value){
        $("#busca-slide").val(value);
        console.log(value);
    }
    $(document).ready(function(){
        $('#form_login_user').submit(function(){
            var dados = $( this ).serialize();
            var data = new Date();
            var minutos = data.getMinutes();
            var hora = data.getHours();
            var calcula = hora*minutos;
            $("#enviar_login_user").hide('fade');
            $("#enviando_login_user").html('......Verificando dados . . .');
            $("#enviando_login_user").removeClass('hide');
            $("#enviando_login_user").show('fade');
            $.ajax({
                type: "POST",
                url: "http://www.gaz.com.br/service/usuario.php",
                data: dados,
                success: function( data ){
                    if(data==0){
                        $(".texto_aviso_login_user").html('Login ou senha inválidos');
                        $(".texto_aviso_login_user").removeClass('hide');
                        $("#email_user_login_user").addClass('required');
                        $("#senha_user_login_user").addClass('required');
                        $(".texto_aviso_login_user").show('fade');
                        $("#enviar_login_user").show('fade');
                        $("#enviando_login_user").html('');
                        $("#enviando_login_user").addClass('hide');
                        $("#enviando_login_user").hide('fade');
                    }else{
                        localStorage.setItem( "loginUser", data);
                        setTimeout(function() {
                            location.reload();
                        }, 200);
                    }
                },
                error: function( data ){
                    alert('Problemas ao logar na página');
                },
        });
        return false;
        });
        $('#form_login_user_mobile').submit(function(){
            var dados = $( this ).serialize();
            var data = new Date();
            var minutos = data.getMinutes();
            var hora = data.getHours();
            var calcula = hora*minutos;
            $("#enviar_login_user_mobile").hide('fade');
            $("#enviando_login_user_mobile").html('Verificando dados . . .');
            $("#enviando_login_user_mobile").removeClass('hide');
            $("#enviando_login_user_mobile").show('fade');
            $.ajax({
                type: "POST",
                url: "http://www.gaz.com.br/service/usuario.php",
                data: dados,
                success: function( data ){
                    if(data==0){
                        $(".texto_aviso_login_user_mobile").html('Login ou senha inválidos');
                        $(".texto_aviso_login_user_mobile").removeClass('hide');
                        $("#email_user_login_user_mobile").addClass('required');
                        $("#senha_user_login_user_mobile").addClass('required');
                        $(".texto_aviso_login_user_mobile").show('fade');
                        $("#enviar_login_user_mobile").show('fade');
                        $("#enviando_login_user_mobile").html('');
                        $("#enviando_login_user_mobile").addClass('hide');
                        $("#enviando_login_user_mobile").hide('fade');
                    }else{
                        localStorage.setItem( "loginUser", data);
                        setTimeout(function() {
                            location.reload();
                        }, 200);
                    }
                },
                error: function( data ){
                    alert('Problemas ao logar na página');
                },
        });
            return false;
        });

        $('#logout_user').submit(function(){
          $("#sair_login_user").attr('disabled', true);
          var dados = $( this ).serialize();
          $.ajax({
              type: "POST",
              url: "http://www.gaz.com.br/service/usuario.php",
              data: dados,
              success: function( data ){
                window.localStorage.removeItem('loginUser');
                setTimeout(function() {
                  location.reload();
                }, 500);
              },
              error: function( data ){
                  $("#sair_login_user").attr('disabled', false);
                  alert('Problemas ao sair da página');
              },
          });
            return false;
        });

        $('#logout_user_mobile').submit(function(){
          $("#sair_login_user_mobile").attr('disabled', 'disabled');
          var dados = $( this ).serialize();
          $.ajax({
              type: "POST",
              url: "http://www.gaz.com.br/service/usuario.php",
              data: dados,
              success: function( data ){
                window.localStorage.removeItem('loginUser');
                setTimeout(function() {
                  location.reload();
                }, 500);
              },
              error: function( data ) {
                  $("#sair_login_user_mobile").attr('disabled', 'disabled');
                  alert('Problemas ao sair da página');
              },
        });

        return false;

        });
    });
</script>

<!-- script que exibe o cabecalho fixo ou esconde conforme rolagem -->
<script type="text/javascript">
    var tam = $(window).width();
    var dif = 0;

    if (tam < 1024){
        var alturaScroll = 120;
        $(window).scroll(function() {
            dif = ($(this).scrollTop() - alturaScroll);
           if (dif > 0 && $(this).scrollTop() > 120) {
                //$('.gaz-section-cabecalho').fadeOut();
                $('.gaz-section-cabecalho').hide();
                alturaScroll = $(this).scrollTop();
            } else {
                $('.gaz-section-cabecalho').show();
                alturaScroll = $(this).scrollTop();
            }
        });
    }
</script>

<!-- Tooltips -->
<!-- <script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/popper.min.js"></script> -->
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://mdbootstrap.com/previews/docs/latest/js/mdb.min.js"></script>
<script>
    // SideNav Initialization
    //$(".button-collapse").sideNav();

    // SideNav Button Initialization
    $(".button-collapse").sideNav();
    // SideNav Scrollbar Initialization
    var sideNavScrollbar = document.querySelector('.custom-scrollbar');
    Ps.initialize(sideNavScrollbar);

</script>





<style>
  .espaco-gaz-header{ margin: 50px auto; }
  .img-center { margin: 0 auto; }

</style>

<section class="gaz-section-nivel1">

  <div class="container gaz-nivel1-wrapper">

    <!--div class="row">

      <div class="col-xs-12">
          <div class="espaco-gaz-header">
              <img class="img-responsive img-center" src="./eleicoes-2018-1.png" alt="Gaz Eleições 2018">
          </div>
      </div>

    </div -->



    <div class="row">
            <div class="col-xs-12">

          <div>


    <!-- ELEIÇÕES GAZ/TINX CSS -->
    <link rel="stylesheet" type="text/css" href="http://www.gaz.com.br/conteudos/resultados/static/css/estilos_eleicoes.css">

    <!-- ELEIÇÕES GAZ/TINX JAVASCRIPT -->
    <script src="http://www.gaz.com.br/conteudos/resultados/static/js/cookies.js"></script>
    <script src="http://www.gaz.com.br/conteudos/resultados/static/js/progressbar.min.js"></script>
    <script src="http://www.gaz.com.br/conteudos/resultados/static/js/rotinas-container.js"></script>



<?php require(__DIR__ . '/../src/views/capa.php'); ?>


        </div>


      </div>

    </div>
  </div> </section>

  <section class="gaz-section-nivel1">

    <div class="container editoria">

      <div class="row lista-de-videos">

        <div class="col-xs-12">
          <div class="hidden-xs gaz-titulo-videos">
            ÚLTIMAS NOTÍCIAS SOBRE AS ELEIÇÕES
          </div>
        </div>

      </div>

    </div> <!-- .container -->

    <!-- div class="container">

      <div class="row">
        <div class="col-md-12 col-xs-12" style="margin: 3em 0;">
            <span id="limite_reg" data-limit="30" class="btn btn-mais-noticias btn-primary form-control" onClick="javascript:carregaPagina(30)">MAIS NOTÍCIAS</span>
        </div>

      </div>
    </div -->

</section> <!-- .gaz-section-nivel1 --><section class="gaz-section-footer">

  <footer>

    <div class="container gaz-footer-borda"></div>


    <div class="container gaz-footer">

      <div class="row">

        <div class="col-xs-12 col-sm-4 col-lg-4">

          <div class="gaz-logo-footer"></div>

        </div>

        <div class="col-sm-8 col-lg-8">

          <div class="row">

            <div id="navbar-footer">

              <ul class="nav navbar-nav nav-social">
                <li><a target="_blank" href="https://www.facebook.com/portalgaz"><i class="fa fa-facebook gaz-social" aria-hidden="true"></i></a></li>
                <li><a target="_blank" href="https://twitter.com/portalgaz"><i class="fa fa-twitter gaz-social" aria-hidden="true"></i></a></li>
                <li><a target="_blank" href="https://instagram.com/portalgaz"><i class="fa fa-instagram gaz-social" aria-hidden="true"></i></a></li>
                <li><a target="_blank" href="https://www.youtube.com/user/portalgaz"><i class="fa fa-youtube gaz-social" aria-hidden="true"></i></a></li>
                <li><a id="whatsapp" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Adicione o número 999130141" data-original-title="WhatsApp Gaz"><i class="fa fa-whatsapp gaz-social" aria-hidden="true"></i></a></li>
              </ul>

            </div> <!-- navbar-footer -->

          </div>

          <div class="row gaz-footer-link bloco1">

            <div class="col-sm-4 col-lg-4">
              <a href="http://www.gaz.com.br/conteudos/anuncie/" target="_parent" title="Anuncie no Portal Gaz">Anuncie no Portal Gaz</a>
            </div>

            <div class="col-sm-4 col-lg-4">
              <a href="http://assinaturas.gaz.com.br/assinatura/index" target="_blank" title="Assine Gazeta do Sul">Assine Gazeta do Sul</a>
            </div>

            <div class="col-sm-4 col-lg-4">
              <a href="http://www.gaz.com.br/conteudos/nos_somos_a_gazeta/" title="Nós somos a Gazeta">Nós somos a Gazeta</a>
            </div>

          </div> <!-- row -->

        </div> <!-- col-lg-8 -->

      </div> <!-- row -->

    </div> <!-- gaz-footer -->


    <div class="container gaz-footer-link bloco2">

      <div class="row">

        <div class="col-xs-6 col-sm-3 col-lg-3">

          <ul class="list-unstyled">
            <li><a href="http://www.gaz.com.br/conteudos/geral/">Geral</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/esportes/">Esportes</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/policia/">Polícia</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/politica/">Política</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/regional/">Regional</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/concursos/">Concursos</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/variedades/">Variedades</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/previsao_do_tempo/">Previsão do tempo</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/cinema/">Cinema</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/colunistas/">Colunistas</a></li>
          </ul>

        </div>

        <div class="col-xs-6 col-sm-3 col-lg-3">

          <ul class="list-unstyled">
            <li><a href="http://www.gaz.com.br/arquivos_biblioteca/2018/07/04/GUIA_2018_1928918d86a708dd24cb7c977ff84ec7.pdf" target="_blank">Guia Socioeconômico</a></li>
            <li><a title="Revista Santa Cruz" href="http://www.editoragazeta.com.br/revistasantacruzdosul/" target="_blank">Revista Santa Cruz</a></li>
            <li><a title="Editora Gazeta" href="http://www.editoragazeta.com.br/" target="_blank">Editora Gazeta</a></li>
            <li><a title="Fundação Gazeta" href="http://www.fundacaogazeta.com.br/" target="_blank">Fundação Gazeta</a></li>
            <li><a href="http://www.viavale.com.br/" target="_blank">Viavale Telecom</a></li>
                        <li><a href="http://www.gaz.com.br/conteudos/especial_seco/iframe.php" target="_self">Série O Quadrilheiro</a></li>
          </ul>

        </div>

        <div class="col-xs-6 col-sm-3 col-lg-3">

          <ul class="list-unstyled">
            <li><a href="http://www.gaz.com.br/conteudos/anuncie/">Anuncie</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/aviso_legal/">Aviso Legal</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/sobre_o_portal_gaz/">Sobre o Portal Gaz</a></li>
            <li><a href="http://www.gaz.com.br/conteudos/fale_conosco/">Fale conosco</a></li>
          </ul>

        </div>

        <div class="col-xs-6 col-sm-3 col-lg-3">

          <ul class="list-unstyled">
            <li><a href="http://www.gazetadaserra.com.br/" target="_blank">Jornal Gazeta da Serra</a></li>
            <li><a href="http://www.gazetadosul.com.br/" target="_blank">Jornal Gazeta do Sul</a></li>
            <li><a href="http://www.gazetaam.com.br/" target="_blank">Rádio Gazeta Am 1180</a></li>
            <li><a href="http://www.grupogaz.com.br/radioam790/" target="_blank">Rádio Rio Pardo Am 790</a></li>
            <li><a href="http://www.gazeta.fm.br/" target="_blank">Rádio Gazeta Fm 101.7</a></li>
            <li><a href="http://www.gazeta98.fm.br/" target="_blank">Rádio Gazeta Fm Sobradinho 98.1</a></li>
          </ul>

        </div>

      </div> <!-- row -->

      <div class="row">

        <div class="col-xs-10 col-md-4 col-md-offset-4 gaz-creditos">

          <p>Desenvolvido e Mantido por<br>Equipe de TI Gazeta Grupo de Comunicações</p>

        </div>

        <div class="col-xs-2 col-md-4 text-right">
           <p class="visible-xs">&nbsp;</p>
           <a href="http://www.gaz.com.br/conteudos/webmail" target="_blank" title="Webmail Gazeta Grupo" alt="Webmail"><img alt="Webmail" src="http://www.gaz.com.br/static/img/icone-email-lock.png" alt="Webmail"></a>
        </div>

      </div><!-- row -->

    </div> <!-- container -->

  </footer>

</section>


  </div>




  <!-- Bootstrap core JavaScript
  ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://www.gaz.com.br/static/js/script.js"></script>
    <script src="http://www.gaz.com.br/static/js/popup_publicidade.js?v14" type="text/javascript"></script>
  <script src="http://www.gaz.com.br/static/js/gaz-pw/bootstrap.min.js"></script>
  <script src="http://www.gaz.com.br/static/js/gaz-pw/ie10-viewport-bug-workaround.js"></script>

  <script src="http://www.gaz.com.br/static/js/util_script.js?v2" type="text/javascript" charset="utf-8"></script>
  <script src="http://www.gaz.com.br/static/js/social-media.js"></script>
  <script src="http://www.gaz.com.br/static/js/banners_dfp_responsivo.js"></script>

  </body>
</html>