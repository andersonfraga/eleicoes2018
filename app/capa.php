<?php

//error_reporting(0);

chdir(getcwd());

require __DIR__ . '/../src/helper.php';
require __DIR__ . '/../src/App.php';

$app = new App;

$_ENV['disable_zero']  = false;
$_ENV['disable_cache'] = true;
$_ENV['cache_seconds'] = 60;

echo $app->run('capa', 1, 'brasil');
