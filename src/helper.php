<?php

function get_file($file)
{
    static $cache;

    $hash = md5($file);

    if (!isset($cache[$hash])) {
        $cache[$hash] = json_decode(file_get_contents($file), true);
    }

    return $cache[$hash];
}

function get_cities()
{
    return get_file(__DIR__ . '/../app/data/cities_sumarized.json');
}

function get_cargos()
{
    return [
        1 => 'Presidente',
        3 => 'Governador',
        5 => 'Senadores',
        6 => 'Deputado Federal',
        7 => 'Deputado Estadual',
    ];
}

function get_candidato($type, $id, $data_votos)
{
    static $data;

    $files = [
        1 => 'br-c0001-e000295-041-f',
        3 => 'rs-c0003-e000297-002-f',
        5 => 'rs-c0005-e000297-003-f',
        6 => 'rs-c0006-e000297-004-f',
        7 => 'rs-c0007-e000297-004-f',
    ];

    if (!isset($files[$type])) {
        return [];
    }

    if (!isset($data[$type][$id])) {
        $file_cand = get_file(__DIR__ . "/../app/data/{$files[$type]}.json");

        $selected = [];

        $abrangencia = $data_votos['Abrangencia'];

        if (isset($data_votos['Abrangencia'][0])) {
            $abrangencia = $data_votos['Abrangencia'][0];
        }

        $votos_totais = $abrangencia['@attributes']['votosValidos'];
        $votos_candidatos = $abrangencia['VotoCandidato'];

        $votos_candidato_item = array_filter($votos_candidatos, function ($item) use ($id) {
            return $item['@attributes']['numeroCandidato'] == $id;
        });

        if (!empty($votos_candidato_item)) {
            $votos_candidato_item = array_shift($votos_candidato_item);
            $votos_candidato_item = $votos_candidato_item['@attributes'];
        }

        if (empty($votos_candidato_item)) {
            $votos_candidato_item = [
                'totalVotos' => 0,
                'classificacao' => -INF,
                'eleito' => 'N',
            ];
        }

        $votos_candidato_item['porcentagem'] = round(($votos_candidato_item['totalVotos']/max($votos_totais,1))*100, 2);

        foreach ($file_cand['Cargo']['Coligacao'] as $item) {
            // Coligações com multiplos partidos
            if (isset($item['Partido']) and !isset($item['Partido'][0])) {
                $item['Partido'] = [$item['Partido']];
            }

            foreach ($item['Partido'] as $siglas) {
                if (!isset($siglas['Candidato'])) {
                    continue;
                }

                // Partidos com multiplos senadores
                if (!isset($siglas['Candidato'][0])) {
                    $siglas['Candidato'] = [$siglas['Candidato']];
                }

                foreach ($siglas['Candidato'] as $candidato) {
                    // Indeferidos
                    if (!isset($candidato['@attributes']['numero'])) {
                        continue;
                    }

                    if ($candidato['@attributes']['numero'] == $id) {
                        $selected = array_merge($votos_candidato_item, [
                            'seqCand'    => $candidato['@attributes']['seqCand'],
                            'nomeUrna'   => $candidato['@attributes']['nomeUrna'],
                            'composicao' => $item['@attributes']['composicao'],
                            'sigla'      => $siglas['@attributes']['sigla'],
                        ]);

                        break;
                    }
                }
            }

            if (!empty($selected)) {
                break;
            }
        }

        if (!empty($selected)) {
            $selected['id'] = $id;
            $data[$type][$id] = $selected;
        }
    }

    return (isset($data[$type][$id])) ? $data[$type][$id] : [];
}

function make_uri(array $query)
{
    $allow = [
        'cargo' => '',
        'local' => '',
        'ordem' => '',
        'tipo'  => '',
    ];

    $actual = array_intersect_key($_GET, $allow);
    return '?' . http_build_query(array_merge($actual, $query));
}

function make_local($local)
{
    $cities = get_cities();

    $selected = array_filter($cities, function ($city) use ($local) {
        return $city['cod'] == $local;
    });

    if (count($selected)) {
        $selected = array_shift($selected);
        return $selected['name'];
    }

    return $local == 'rs' ? 'Rio Grande do Sul' : 'Brasil';
}

function make_cargo($cargo)
{
    $list = get_cargos();
    return (isset($list[$cargo])) ? $list[$cargo] : 'Presidente';
}

function make_candidatos($data, $cargo, $data_votos)
{
    $list = [];

    foreach ($data as $candidato) {
        $candidato = $candidato['@attributes'];
        $item = get_candidato($cargo, $candidato['numeroCandidato'], $data_votos);

        if (empty($item) || ($_ENV['disable_zero'] && $candidato['totalVotos'] == 0)) {
            continue;
        }

        $list[] = array_merge($item, $candidato);
    }

    uasort($list, function ($a, $b) {
        if ($a['totalVotos'] == $b['totalVotos']) {
            return strnatcmp($a['nomeUrna'], $b['nomeUrna']);
        }

        return $a['classificacao'] > $b['classificacao'] ? 1 : -1;
    });

    return $list;
}