<?php

class App
{
    function run($tipo, $cargo, $local)
    {
        if ($tipo == 'capa') {
            return $this->capa($local);
        }

        return $this->expandido($cargo, $local);
    }

    public function capa($local)
    {
        return $this->cache('capa-' . $local, function () use ($local) {
            // rs-
            $local_pres = $local_gov = 'rs';

            // br-
            // local_gov mantém rs pq não há brasil ;)
            /*if ($local == 'brasil') {
                $local_pres = 'br';
            }
            // rs85212-
            else if ($local != 'rs') {
                $local_pres = $local_gov = 'rs' . $local;
            }*/

            $pres_hash = "{$local_pres}-c0007-e000297";
            $pres_data = get_file(__DIR__ . "/../app/data/{$pres_hash}-v.json");

            $gov_hash = "{$local_gov}-c0003-e000297";
            $gov_data = get_file(__DIR__ . "/../app/data/{$gov_hash}-v.json");

            //trigger_error("{$pres_hash} - {$gov_hash}");

            $cities      = get_cities();
            $localizacao = make_local($local);

            $abrangencia = $pres_data['Abrangencia'];

            if (is_numeric($local)) {
                $abrangencia = $abrangencia[0];
            }

            $presidente = make_candidatos(
                $pres_data['Abrangencia']['VotoCandidato'] ?? [],
                7,
                $pres_data
            );
            $governador = make_candidatos(
                $gov_data['Abrangencia']['VotoCandidato'] ?? [],
                3,
                $gov_data
            );

            $secoesTotalizadas = $secoesNaoTotalizadas = $horaTotalizacao = 0;
            $eleitoradoApurado = $eleitoradoNaoApurado = $abstencao = 0;
            $votosTotalizados = $votosEmBranco = $votosNulos = $votosValidos = 0;

            extract($abrangencia['@attributes']);

            include('views/capa-lista.php');
        });
    }

    public function expandido($cargo, $local)
    {
        // rs-
        $id = 'rs';

        // br-
        if ($local == 'brasil') {
            $id = 'br';

            // Não existe outro cargo a nível brasil né...
            if ($cargo != 1) {
                $id = $local = 'rs';
            }
        }
        // rs85212-
        else if ($local != 'rs') {
            $id .= $local;
        }

        $eleicao = ($cargo == 1) ? 295 : 297;

        $hash = "{$id}-c000{$cargo}-e000{$eleicao}";

        return $this->cache_by_json($hash, function ($data) use ($local, $cargo) {
            $cities      = get_cities();
            $localizacao = make_local($local);
            $cargo_nome  = make_cargo($cargo);

            $abrangencia = $data['Abrangencia'];

            /*$apuracao_candidatos = [];

            foreach ($data['Abrangencia'] as $_item) {
                if (isset($_item['VotoCandidato'])) {
                    $apuracao_candidatos += $_item['VotoCandidato'];
                }
            }*/

            if (is_numeric($local)) {
                $abrangencia = $abrangencia[0];
            }

            $secoesTotalizadas = $secoesNaoTotalizadas = $horaTotalizacao = 0;
            $eleitoradoApurado = $eleitoradoNaoApurado = $abstencao = 0;
            $votosTotalizados = $votosEmBranco = $votosNulos = $votosValidos = 0;

            $votacao = make_candidatos($abrangencia['VotoCandidato'], $cargo, $data);

            extract($abrangencia['@attributes']);

            include('views/index.php');
        });
    }

    public function cache_by_json($hash, $content)
    {
        $cache = __DIR__ . "/../app/cache/{$hash}.html";

        $read_file = function ($hash) use ($content) {
            $data = get_file(__DIR__ . "/../app/data/{$hash}-v.json");

            ob_start();
            $content($data);
            return ob_get_clean();
        };

        if ($_ENV['disable_cache']) {
            return $read_file($hash);
        }

        if (!is_file($cache) || filemtime($cache) < (time() - $_ENV['cache_seconds'])) {
            $result = $read_file($hash);
            file_put_contents($cache, $result);
            return $result;
        }

        return file_get_contents($cache);
    }

    public function cache($hash, $content)
    {
        $cache_file = __DIR__ . "/../app/cache/{$hash}.html";

        if ($_ENV['disable_cache']) {
            return $content();
        }

        if (!is_file($cache_file) || filemtime($cache_file) < (time() - $_ENV['cache_seconds'])) {
            ob_start();
            $content();
            $sandbox = ob_get_clean();

            file_put_contents($cache_file, $sandbox);
            return $sandbox;
        }

        return file_get_contents($cache_file);
    }
}