<?php

exit;

function strlow($str)
{
    $itens = [$str];
    if (stripos($str, ' ') !== false) {
        $itens = explode(' ', $str);
    }

    $articles = ['da', 'das', 'de', 'do', 'dos'];

    return implode(' ', array_map(function ($word) use ($articles) {
        $word = mb_strtolower($word);

        if (!in_array($word, $articles)) {
            $word = mb_strtoupper(mb_substr($word, 0, 1)) . mb_substr($word, 1);
        }

        return $word;
    }, $itens));
}

$storage  = __DIR__ . "/../../storage/";
$datapath = __DIR__ . "/../../app/data/";

file_put_contents($storage . 'cities.json', fopen(
    'http://divulga.tse.jus.br/2018/divulgacao/oficial/295/config/rs/rs-e000295-w.js',
    'r'
));

$cities = json_decode(file_get_contents($storage . 'cities.json'), true);

$content = [];

foreach ($cities as $city) {
    $content[] = [
        'cod'  => $city['cd'],
        'name' => strlow($city['nm']),
    ];
}

file_put_contents($datapath . 'cities_sumarized.json', json_encode($content));