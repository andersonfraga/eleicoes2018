<?php

$datapath = __DIR__ . "/../../app/data/";
$storage  = __DIR__ . "/../../storage/";

foreach (new DirectoryIterator($storage) as $file) {
    if (!$file->isFile()) {
        continue;
    }

    if ($file->getExtension() != 'zip') {
        continue;
    }

    $zip = new ZipArchive;
    $res = $zip->open($file->getPathname());

    if ($res === true) {
        $zip->extractTo($datapath);
        $zip->close();
    }

    $basenamexml  = $file->getBasename('.zip') . '.xml';
    $basenamesig  = $file->getBasename('.zip') . '.sig';
    $basenamejson = $file->getBasename('.zip') . '.json';

    if (is_file($datapath . $basenamexml)) {
        $xml   = simplexml_load_file($datapath . $basenamexml);
        $json  = json_encode($xml);
        $array = json_decode($json, true);

        file_put_contents($datapath . $basenamejson, $json);

        echo "{$file->getPathname()} => {$datapath}{$basenamejson}\n";

        unlink($file->getPathname());
        unlink($datapath . $basenamexml);
        unlink($datapath . $basenamesig);
    }
    else {
        echo "{$datapath}{$basenamexml}\n";
    }
}