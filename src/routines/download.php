<?php

exit;

set_time_limit(0);

function download($source, $dest, $using_name = false)
{

    $tmp = tempnam(__DIR__ . "/../../storage/tmp/", mt_rand());

     //This is the file where we save the    information
    $fp = fopen ($tmp, 'w+');
    //Here is the file we are downloading, replace spaces with %20
    $ch = curl_init(str_replace(" ","%20", $source));
    curl_setopt($ch, CURLOPT_TIMEOUT, 50);
    // write curl response to file
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    // get curl response
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);

    //file_put_contents($tmp, fopen($source, 'r'));

    if ($using_name) {
        $dest = $dest . basename($source);
    }

    file_put_contents('/tmp/eleicoes.log', "{$source} => {$dest}\n", FILE_APPEND);
    echo "{$source} => {$dest}\n";

    rename($tmp, $dest);
}

$list_cargo = [
    '0001' => 295,
    '0003' => 297,
    '0005' => 297,
    '0006' => 297,
    '0007' => 297,
];

$storage = __DIR__ . "/../../storage/";
$storage_cities = "{$storage}/cities.json";

$list_city = json_decode(file_get_contents($storage_cities), true);

$uri = "http://divulga.tse.jus.br/2018/divulgacao/oficial/%s/distribuicao/%s/%s.%s";

download(
    sprintf($uri, 295, 'br', 'br-c0001-e000295-v', 'zip'),
    $storage,
    true
);

// Eu sei eu sei
// php download.php 0001
// php download.php 0003
// php download.php 0005
// php download.php 0006
// php download.php 0007
if (isset($argv[1]) and isset($list_cargo[$argv[1]])) {
    $list_cargo = [
        $argv[1] => $list_cargo[$argv[1]],
    ];

    echo "Buscando para {$argv[1]}\n";
}

// http://divulga.tse.jus.br/2018/divulgacao/oficial/295/distribuicao/br/br-c0001-e000295-t.zip
foreach ($list_cargo as $cargo => $eleicao) {
    $cargo = str_pad($cargo, 4, '0', STR_PAD_LEFT);
    download_file($eleicao, 'rs', "rs-c{$cargo}-e000{$eleicao}-v", $storage);

    foreach ($list_city as $_city) {
        $city = $_city['cd'];
        download_file($eleicao, 'rs', "rs{$city}-c{$cargo}-e000{$eleicao}-v", $storage);
    }
}

function download_file($eleicao, $dir, $arquivo, $storage)
{
    global $uri;

    download(
        sprintf($uri, $eleicao, $dir, $arquivo, 'zip'),
        $storage,
        true
    );
}