<?php

exit;

$uri = "http://divulga.tse.jus.br/2018/divulgacao/oficial/%3\$d/distribuicao/%1\$s/%1\$s-c000%2\$d-e000%3\$d-%4\$s-f.zip";

$itens = [
    ['br', '1', '295', '041'],
    ['rs', '3', '297', '002'],
    ['rs', '5', '297', '003'],
    ['rs', '6', '297', '004'],
    ['rs', '7', '297', '004'],
];

$storage  = __DIR__ . "/../../storage/";

foreach ($itens as $_item) {
    $file = vsprintf($uri, $_item);
    $name = basename($file);

    echo $file . PHP_EOL;
    file_put_contents($storage . $name, fopen($file, 'r'));
}