<link rel="stylesheet" type="text/css" href="http://www.gaz.com.br/conteudos/resultados/static/css/estilos_eleicoes.css">

<!-- ELEIÇÕES GAZ/TINX JAVASCRIPT -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="http://www.gaz.com.br/conteudos/resultados/static/js/cookies.js"></script>
<script src="http://www.gaz.com.br/conteudos/resultados/static/js/progressbar.min.js"></script>
<script src="http://www.gaz.com.br/conteudos/resultados/static/js/rotinas-home.js"></script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" />

<style type="text/css" media="screen">
.selectCapaEleicoes {
    -moz-appearance: none;
    -webkit-appearance: none;
    outline: none;
    background-color: #0159b0;
    color: #FFF;
    border: none;
    padding: 6px;
}
.selectCapaEleicoes::-ms-expand {
  display: none;
}
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
}


@media all and (min-width: 640px) {
    .vcenter {
        margin: 0 10px;
    }
    .blk_apur {
        max-width: 315px;
    }
}

@media all and (max-width: 640px) {
    .btn_eleicoes_apur {
        text-align: center;
    }
    .selectCapaEleicoes {
        max-width: 180px;
    }
}


</style>

<section>
     <div class="container" style="padding:0;">
          <div class="row" style="margin-top: 10px; margin-bottom:10px;">
               <div class="col-xs-12">
                    <div id="eleicoesHomeArea">

    <div id="apuracaoContainer">
        <div class="row">
            <div class="col-xs-12 col-md-1 vcenter">
                <img width="90px" src="http://www.gaz.com.br/apuracaoeleicoes2018/eleicoes-2018-1.png" />
            </div>
            <!-- div class="col-xs-12 col-md-4 text-center vcenter blk_apur">
                <span id="apuracaoTitle" style="text-transform: uppercase; font-size:20px; padding-right: 0;">VOTOS</span>
                <select id="clk_local_capa" class="selectCapaEleicoes text-uppercase" style="padding-left: 0;">
                    <option value="brasil">no Brasil</option>
                    <option value="rs">no Rio Grande do Sul</option>
                    <option value="88390">em Santa Cruz do Sul</option>
                </select>
                <i class="fa fa-caret-down"></i>
            </div -->
            <div class="col-xs-12 col-md-10 text-left vcenter btn_eleicoes_apur">
                <a href="http://www.gaz.com.br/apuracaoeleicoes2018" class="btn btn-success" style="color:#fff;">Apuração completa</a>
            </div>
        </div>
    </div>

    <script>
        /* wait for jquery to load */
        window.wait4j=function(a){if(window.jQuery){a.call(this);}else{setTimeout(function(){window.wait4j(a);},1);}};
    </script>

    <script type="text/javascript" charset="utf-8">
        wait4j(function() {
            $('#clk_local_capa').change(function () {
                let selected = $(this).find(":selected").val();

                //$("#divLoadingData").show();

                $.get('http://www.gaz.com.br/apuracaoeleicoes2018/index.php?tipo=capa&local=' + selected, function(data) {
                    //$("#divLoadingData").hide();
                    $("#divDados").html(data);
                })
            });

            $.get('http://www.gaz.com.br/apuracaoeleicoes2018/index.php?tipo=capa', function(data) {
                //$("#divLoadingData").hide();
                $("#divDados").html(data);
            })
        })
    </script>

    <div class="linhaVerde"></div>
    <div id="divDadosContainer">
        <div id="divDados">


        </div>
        <!-- div id="divLoadingData">
            <center><img height="100" src="http://gaz.com.br/conteudos/resultados/static/img/carregando.gif"></center>
        </div -->
    </div>
    <div class="linhaVerde"></div>
</div>
                    </div>
          </div>
     </div>
</section><!-- banner premium -->
<section id="section_banner_premium" class="container-fluid">
     <div class="row">

        <div class="col-md-12 hidden-xs hidden-sm">
             <div class="banner_top_premium" style="text-align:center;">


                <!-- /19167842/GAZ-970x250-Topo-Premium -->
                <div id='div-gpt-ad-1502978839662-0'>
                <script type='text/javascript'>
                  googletag.cmd.push(function() { googletag.display('div-gpt-ad-1502978839662-0'); });
                </script>
                </div>

             </div>
        </div>

     </div>
</section>