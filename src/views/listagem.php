
<div class="col-md-1 col-sm-5 col-xs-5">
  <div class="dropdown">
    <button class="btn btn-success dropdown-toggle" type="button" id="data_city" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      Cargo
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" aria-labelledby="data_city">
     <?php foreach(get_cargos() as $_id => $_cargo): ?>
      <li><a href="<?=make_uri(['cargo' => $_id])?>"><?=$_cargo?></a></li>
     <?php endforeach; ?>
    </ul>
  </div>
</div>
<div class="col-md-1 col-sm-7 col-xs-7 text-right">
  <div class="dropdown dropdown-menu-right">
    <button class="btn btn-success dropdown-toggle" type="button" id="data_city" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      localização
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu dropdown-menu-right pre-scrollable " aria-labelledby="data_city">
      <li><a href="<?=make_uri(['local' => 'brasil'])?>">Brasil</a></li>
      <li><a href="<?=make_uri(['local' => 'rs'])?>">Rio Grande do Sul</a></li>
      <li><a href="<?=make_uri(['local' => '88390'])?>">Santa Cruz do Sul</a></li>
      <li role="separator" class="divider"></li>
      <?php
      /*foreach ($cities as $city):
      ?>
      <li><a href="<?=make_uri(['local' => $city['cod']])?>"><?=$city['name']?></a></li>
      <?php endforeach;*/ ?>
    </ul>
  </div>
</div>


<div class="col-md-8 col-sm-12 col-xs-12">
    <h2 class="hidden-md hidden-lg text-center text-uppercase">APURAÇÃO PARA <?=$cargo_nome?></h2>
    <h2 class="hidden-xs hidden-sm text-center text-uppercase" style="margin:0">APURAÇÃO PARA <?=$cargo_nome?></h2>
</div>


<div class="col-md-2 hidden-xs hidden-sm">&nbsp;</div>

<div class="col-lg-12 col-sm-12 col-xs-12 not-container" style="margin-top:10px">
    <h1 class="text-center text-uppercase" style="position: inherit;"><?=$localizacao?></h1>
</div>

<div class="not-container">
    <div class="row">
        <table id="tableCandidatos" class="tableChart table table-condensed" cellpadding="0" cellspacing="4">

<?php
foreach ($votacao as $candidato):
    $uri_fotos = ($cargo == 1) ? '295/fotos/br' : '297/fotos/rs';
?>
<tr class="tr1">
    <td class="tdFoto" rowspan="2" nowrap="nowrap"><img height="72" src="http://divulga.tse.jus.br/2018/divulgacao/oficial/<?=$uri_fotos?>/<?=$candidato['seqCand']?>.jpeg"></td>
    <td class="tdCandidato"> <span class="candidatoNome"><?=$candidato['nomeUrna']?></span> <span class="candidatoPartido" title="<?=$candidato['composicao']?>"><br />
        <?=$candidato['id']?> - <?=$candidato['sigla']?></span></td>
    <td class="tdVotos" align="right"><span class="votosPct">
        <?=$candidato['porcentagem']?>%
    </span><br /><span class="votosQtd"><?=$candidato['totalVotos']?> votos</span></td>
</tr>
<tr class="tr2">
    <td colspan="2" class="prefeitoProgress">
        <div class="progress">
        <?php if ($candidato['eleito'] == 'S'): ?>
            <div class="progress-bar  progress-bar-success" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                <?=($cargo <= 3) ? 'Segundo turno': 'Eleito'?>
            </div>
        <?php else: ?>
            <div class="progress-bar" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                <span class="sr-only"><?=$candidato['porcentagem']?>%</span>
            </div>
        <?php endif; ?>
        </div>
    </td>
</tr>
<?php endforeach;

if (empty($votacao)) {
    echo '<h4 class="text-center">Apuração ainda não inicializada. Previsão de início as 17 horas</h4>';
}
?>

            </table>
        </div>
    </div>
</div>

<div class="col-lg-12 col-sm-12 col-xs-12">
    <div class="col-sm-4 col-md-4">
        <table class="table table-condensed table-stats small">
            <tbody><tr class="info">
                <th width="35%">SEÇÕES</th><td width="65%"><?=($numSecoes = ($secoesTotalizadas + $secoesNaoTotalizadas))?></td>
            </tr>
            <tr>
                <th>TOTALIZADAS</th><td><?=$secoesTotalizadas?> (<?=round(($secoesTotalizadas/$numSecoes)*100, 2)?>%)</td>
            </tr>
            <tr>
                <th>ATUALIZAÇÃO</th><td class="lastData"><?=$horaTotalizacao?></td>
            </tr>
        </tbody></table>
    </div>
    <div class="col-sm-4 col-md-4">
        <table id="" class="table table-condensed table-stats small">
            <tbody><tr class="info">
                <th width="35%">ELEITORADO</th><td width="65%"><?=($numEleitores = ($eleitoradoApurado + $eleitoradoNaoApurado))?></td>
            </tr>
            <tr>
                <th>APURADO</th><td><?=$eleitoradoApurado?> (<?=round(($eleitoradoApurado/$numEleitores)*100, 2)?>%)</td>
            </tr>
            <tr>
                <th>ABSTENÇÕES</th><td><?=$abstencao?> (<?=round(($abstencao/$numEleitores)*100, 2)?>%)</td>
            </tr>
        </tbody></table>
    </div>
    <div class="col-sm-4 col-md-4">

        <table id="" class="table table-condensed table-stats small">
            <tbody><tr class="info">
                <th width="35%">VOTOS</th><td width="65%"><?=$votosTotalizados = max($votosTotalizados,1);?></td>
            </tr>
            <tr>
                <th>BRANCOS</th><td><?=$votosEmBranco?> (<?=round(($votosEmBranco/$votosTotalizados)*100, 2)?>%)</td>
            </tr>
            <tr>
                <th>NULOS</th><td><?=$votosNulos?> (<?=round(($votosNulos/$votosTotalizados)*100, 2)?>%)</td>
            </tr>
            <tr>
                <th>VÁLIDOS</th><td><?=$votosValidos?> (<?=round(($votosValidos/$votosTotalizados)*100, 2)?>%)</td>
            </tr>
        </tbody></table>
    </div>
</div>