<div class="not-container" style="height: 400px; min-height: 50%; max-height: 400px; overflow-y: scroll; overflow-x: hidden;">
    <div class="row">

<div class="col-xs-12 col-sm-12 col-md-5">
    <h1 class="text-center text-uppercase" style="position: inherit; margin-bottom:20px;">Presidente</h1>
    <table id="tableCandidatos" class="tableChart table table-condensed" cellpadding="0" cellspacing="4">

<?php
foreach ($presidente as $candidato):
    $uri_fotos = '295/fotos/br'; // : '297/fotos/rs';
?>
<tr class="tr1">
    <td class="tdFoto" rowspan="2" nowrap="nowrap"><img height="72" src="http://divulga.tse.jus.br/2018/divulgacao/oficial/<?=$uri_fotos?>/<?=$candidato['seqCand']?>.jpeg"></td>
    <td class="tdCandidato"> <span class="candidatoNome"><?=$candidato['nomeUrna']?></span> <span class="candidatoPartido" title="<?=$candidato['composicao']?>"><br />
        <?=$candidato['id']?> - <?=$candidato['sigla']?></span></td>
    <td class="tdVotos" align="right"><span class="votosPct">
        <?=$candidato['porcentagem']?>%
    </span><br /><span class="votosQtd"><?=$candidato['totalVotos']?> votos</span></td>
</tr>
<tr class="tr2">
    <td colspan="2" class="prefeitoProgress">
        <div class="progress">
        <?php if ($candidato['eleito'] == 'S'): ?>
            <div class="progress-bar  progress-bar-success" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                Eleito
            </div>
        <?php else: ?>
            <div class="progress-bar" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                <span class="sr-only"><?=$candidato['porcentagem']?>%</span>
            </div>
        <?php endif; ?>
        </div>
    </td>
</tr>
<?php endforeach; ?>
</table>
</div>
<div class="col-xs-12 col-sm-12 col-md-5">
    <h1 class="text-center text-uppercase" style="position: inherit; margin-bottom:20px;">Governo do Rio Grande do Sul</h1>
    <table id="tableCandidatos" class="tableChart table table-condensed" cellpadding="0" cellspacing="4">
<?php
foreach ($governador as $candidato):
    $uri_fotos = '297/fotos/rs';
?>
<tr class="tr1">
    <td class="tdFoto" rowspan="2" nowrap="nowrap"><img height="72" src="http://divulga.tse.jus.br/2018/divulgacao/oficial/<?=$uri_fotos?>/<?=$candidato['seqCand']?>.jpeg"></td>
    <td class="tdCandidato"> <span class="candidatoNome"><?=$candidato['nomeUrna']?></span> <span class="candidatoPartido" title="<?=$candidato['composicao']?>"><br />
        <?=$candidato['id']?> - <?=$candidato['sigla']?></span></td>
    <td class="tdVotos" align="right"><span class="votosPct">
        <?=$candidato['porcentagem']?>%
    </span><br /><span class="votosQtd"><?=$candidato['totalVotos']?> votos</span></td>
</tr>
<tr class="tr2">
    <td colspan="2" class="prefeitoProgress">
        <div class="progress">
        <?php if ($candidato['eleito'] == 'S'): ?>
            <div class="progress-bar  progress-bar-success" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                Eleito
            </div>
        <?php else: ?>
            <div class="progress-bar" role="progressbar" data-tinx-pct="1" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?=$candidato['porcentagem']?>%;">

                <span class="sr-only"><?=$candidato['porcentagem']?>%</span>
            </div>
        <?php endif; ?>
        </div>
    </td>
</tr>
<?php endforeach; ?>
</table>
</div>

<div class="col-xs-12 col-sm-12 col-md-2">
    <table class="table table-condensed table-stats small">
        <tbody>
        <tr>
            <th width="35%">SEÇÕES</th><td width="65%"><?=($numSecoes = ($secoesTotalizadas + $secoesNaoTotalizadas))?></td>
        </tr>
        <tr>
            <th>TOTALIZADAS</th><td><?=$secoesTotalizadas?> (<?=round(($secoesTotalizadas/$numSecoes)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th width="35%">ELEITORADO</th><td width="65%"><?=($numEleitores = ($eleitoradoApurado + $eleitoradoNaoApurado))?></td>
        </tr>
        <tr>
            <th>APURADO</th><td><?=$eleitoradoApurado?> (<?=round(($eleitoradoApurado/$numEleitores)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th>ABSTENÇÕES</th><td><?=$abstencao?> (<?=round(($abstencao/$numEleitores)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th width="35%">VOTOS</th><td width="65%"><?=$votosTotalizados = max($votosTotalizados,1);?></td>
        </tr>
        <tr>
            <th>BRANCOS</th><td><?=$votosEmBranco?> (<?=round(($votosEmBranco/$votosTotalizados)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th>NULOS</th><td><?=$votosNulos?> (<?=round(($votosNulos/$votosTotalizados)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th>VÁLIDOS</th><td><?=$votosValidos?> (<?=round(($votosValidos/$votosTotalizados)*100, 2)?>%)</td>
        </tr>
        <tr>
            <th>ATUALIZAÇÃO</th><td class="lastData"><?=$horaTotalizacao?></td>
        </tr>
    </tbody></table>

    <a href="http://www.gaz.com.br/apuracaoeleicoes2018" class="btn btn-success" style="color:#fff;">Apuração completa</a>
</div>

    </div>
</div>